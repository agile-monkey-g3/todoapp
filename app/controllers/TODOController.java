package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import models.Todo;
import play.libs.Json;
import play.mvc.*;

import java.util.ArrayList;
import java.util.List;

import static play.mvc.Results.ok;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class TODOController extends Controller {

    @BodyParser.Of(BodyParser.Json.class)
    public Result create() {
        JsonNode json = request().body().asJson();
        String text = json.findPath("text").textValue();
        if(text == null){
            return badRequest("Missing parameter [text]");
        } else {
            Todo todo= new Todo();
            todo.text = text;
            todo.save();
            return created();
        }
    }

    public Result update(String id) {
        return Results.TODO;
    }

    public Result destroy(String id) {
        return Results.TODO;
    }

    public Result show(String id) {
        try{
            Long idL = Long.parseLong(id);
            try{
                Todo todo = Todo.find.byId(idL);
                return ok(Json.toJson(todo));
            }catch(Exception e){
                ObjectNode error = Json.newObject();
                error.put("error", "todo not found");
                return ok(error);
            }
        }catch (Exception e){
            ObjectNode error = Json.newObject();
            error.put("error", "id is not a number");
            return badRequest(error);
        }
    }

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        List<Todo> todos = Todo.find.all();
        return ok(Json.toJson(todos));
    }

}
