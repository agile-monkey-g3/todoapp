package models;

import java.util.*;
import javax.persistence.*;

import io.ebean.*;
import play.data.format.*;
import play.data.validation.*;

@Entity
public class Todo extends Model{
    @Id
    @GeneratedValue
    @Constraints.Min(10)
    public Long id;

    @Constraints.Required
    public String text;

    public static final Finder<Long, Todo> find = new Finder<>(Todo.class);
}
