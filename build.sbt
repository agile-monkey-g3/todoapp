name := """TODOapp"""
organization := "com.example"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.2"

libraryDependencies += guice

//JDBC
libraryDependencies ++= Seq(
  javaJdbc,
  "org.postgresql" % "postgresql" % "9.4-1206-jdbc42"
)
//Ebeans
lazy val myProject = (project in file("."))
  .enablePlugins(PlayJava, PlayEbean)