# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table todo (
  id                            bigserial not null,
  text                          varchar(255),
  constraint pk_todo primary key (id)
);


# --- !Downs

drop table if exists todo cascade;

